var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'eval',
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/client/jsx/client'
  ],
  output: {
    path: path.join(__dirname, 'dist/client/js'),
    filename: 'bundle.js',
    publicPath: '/js/'
  },
  devServer:{
    contentBase: path.join(__dirname, 'src/client/public'),
    host: '0.0.0.0',
    hot: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'src/client')
    }]
  }
};
