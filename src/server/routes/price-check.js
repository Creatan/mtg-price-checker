const router = require('express').Router()
const Cards = require('../models/cards')
const MCM_API = require('../mcm-api/api')

//TODO: Move to utils?
function groupCardsByName(cards){
  return cards.reduce((carry, item) => {
      if(carry.indexOf(item.name) === -1){
        carry.push(item.name)
      }
      return carry
    }, [])
}
function replaceSetName(card){
  const replacer = {
    'Limited Edition Alpha': 'Alpha',
    'Limited Edition Beta': 'Beta',
    'Unlimited Edition': 'Unlimited',
    'Revised Edition': 'Revised',
    'Magic 2014 Core Set': 'Magic 2014',
    'Magic 2015 Core Set': 'Magic 2015',


  }
  return replacer[card.cardSet[0].name] || card.cardSet[0].name
}

function parseCardInfo(cards, data){
  return cards.map(card => {
    card.cardSet[0].name = replaceSetName(card)
    const priceData = data.product.find(product => {
      return product.expansion === card.cardSet[0].name
    })

    if(typeof priceData === 'undefined'){
      console.log('SetNotFound:',card.cardSet[0].name)
      return {}
    }
    return {
      name: card.name,
      set: card.setCode,
      setName: card.cardSet[0].name,
      priceData: {
        low: priceData.priceGuide.LOW,
        avg: priceData.priceGuide.AVG,
        trend: priceData.priceGuide.TREND
      }
    }
  })
}
async function fetchCardPrice(req, res){
  const cardName = req.query.cardName
  let response = {}
  try{
    const cards = await Cards.findByName(cardName)
    if(cards.length > 0){
      const names = groupCardsByName(cards)
      //lets find prices
      for(let name of names){
        const priceData = await MCM_API.getProductPrices(name)
        if(priceData.error){
          console.log('error',priceData.error)
          throw new Error(priceData.error)
        }
        else{
          const cardPrices = parseCardInfo(cards, priceData)
          response = {cardPrices}
        }
      }
    }
  }
  catch(e){
    console.log(e)
    response = {error: e.message}
  }

  res.json(response)
}

router.get('/price', fetchCardPrice)
module.exports = router
