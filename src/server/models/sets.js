'use strict'

const mongoose = require('mongoose')

let SetSchema = new mongoose.Schema({
  name: String,
  code: String,
  gathererCode: String,
  releaseDate: Date,
  border: String,
  type: String,
  mkm_name: String,
  mkm_id: Number
})

module.exports = mongoose.model('Sets', SetSchema)
