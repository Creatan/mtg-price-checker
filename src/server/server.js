const mongoose = require('mongoose')
const express = require('express')

const config = require('../../config')
const priceCheckRouter = require('./routes/price-check')

module.exports = (port) => {
  const app = express()
  mongoose.connect(`mongodb://${config.DB_USER}:${config.DB_PASS}@${config.DB_URI}`)
  mongoose.Promise = require('bluebird')

  app.use(express.static(__dirname + '/../client/public'))

  // app.use('/', (req, res) => {
  //   return res.sendFile('index.html', { root: __dirname + '/../client/public'})
  // })

  app.use('/api', priceCheckRouter)

  app.listen(port || 3000)
}
