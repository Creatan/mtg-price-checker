#!/bin/sh

wget http://mtgjson.com/json/AllSets.json.zip -O ./src/import/data/AllSets.json.zip
unzip ./src/import/data/AllSets.json.zip -d ./src/import/data
rm -f ./src/import/data/AllSets.json.zip
