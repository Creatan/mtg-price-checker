'use strict'
require('babel-register')
require('babel-polyfill')

const mongoose = require('mongoose')
const MCM_API = require('./mcm-api/api')
const Cards = require('./models/cards')
const config = require('../config')

mongoose.connect(`mongodb://${config.DB_USER}:${config.DB_PASS}@${config.DB_URI}`)

const cardName = 'Sylvan advocate'
Cards.findByName(cardName, (err, cards) => {
  if(err){
    console.log('Error ' + err)
    process.exit(1)
  }

  if(cards.length === 0){
    console.log(`No cards with name ${cardName} found`)
    process.exit(0)
  }
  const names = cards.reduce((carry, item) => {
    if(carry.indexOf(item.name) > -1){
      carry.push(item.name)
    }
    return carry
  }, [])
  console.log(names)
  process.exit(0)
  mongoose.disconnect()
  //lets find prices
  MCM_API.getProductPrices().then(function(data){
    if(data.error) console.log('error',data.error)
    else{
      console.log('data',data)
      cards.forEach(card => {
        const priceData = data.product.find(product => {
          return product.expansion === card.cardSet[0].name
        })
        console.log(`${card.name} ${card.setCode} ${card.cardSet[0].name} \n\t low: ${priceData.priceGuide.LOW} \n\t avg: ${priceData.priceGuide.AVG} \n\t trend: ${priceData.priceGuide.TREND}`)
      })
    }
  })
})


// MCM_API.getProductPrices(cardName).then(function(data){
//   if(data.error){
//     console.log(data.error)
//   }
//   else{
//     data.product.forEach(product => {
//       console.log(`${cardName} - ${product.expansion}: \n\t low: ${product.priceGuide.LOW} \n\t avg: ${product.priceGuide.AVG} \n\t trend: ${product.priceGuide.TREND}`)
//     })
//   }
// })
