'use strict'
const request = require('request-promise')
const config = require('../../config')
const Promise = require('bluebird')
const redis = require('redis')
Promise.promisifyAll(redis.RedisClient.prototype)

const API = 'https://www.mkmapi.eu/ws/v1.1/output.json/'
const LANG = 1 //language id for mcm
const MTG = 1 //gameid for mcm
const client = redis.createClient()

client.on('error', err => {
  console.log('Error', err)
})

async function makeRequest(url){
  let oauth = {
    consumer_key: config.APP_TOKEN,
    consumer_secret: config.APP_SECRET,
    token: '',
    token_secret: '',
    realm: url
  }

  return request.get({
    url:url,
    oauth:oauth
  })
}

async function getMetaData(name){

  const url = API + `metaproducts/${name}/${MTG}/${LANG}`
  let data = {}
  try{
     data = await makeRequest(url)

  }
  catch(error){
    console.log('metadata',error)
    if(error.statusCode === 401){
      data = '{"error":"Unauthorized"}'
    }
    else{
      data = error.response.body
    }
  }
  return JSON.parse(data)
}

async function getPriceData(name){
  const metaData = await getMetaData(name)
  if(metaData.error){
    return metaData
  }

  const url = API + 'product/'+ metaData.metaproduct.products.idProduct[0]

  let priceData = {}
  try{
    priceData = await makeRequest(url)
  }
  catch(error){
    if(error.statusCode === 401){
      priceData = '{"error":"Unauthorized"}'
    }
    else{
      priceData = error.response.body
    }
  }

  return JSON.parse(priceData)
}

async function getProductPrices(name){
  const url = `${API}products/${encodeURIComponent(name)}/${MTG}/${LANG}/false`
  const result = await client.getAsync(encodeURIComponent(name))

  let priceData = {}
  if(result){
    console.log('from cache')
    priceData = result
  }
  else{
    try{
      console.log('fresh request')
      priceData = await makeRequest(url)

      // save data to cache with expiration timer of one hour
      client.setex(encodeURIComponent(name), 60*60, priceData)
    }
    catch(error){
      if(error.statusCode === 401){
        console.log(error)
        priceData = JSON.stringify({error:"Unauthorized"})
      }
      else{
        priceData = error.response.body
      }
    }
  }

  return JSON.parse(priceData)
}

module.exports = {
  getMetaData,
  getPriceData,
  getProductPrices
}
