'use strict'

const Promise = require('bluebird')
const readFile = Promise.promisify(require('fs').readFile)

const Cards = require('../models/cards')
const Sets = require('../models/sets')

//TODO: TEST THAT THIS STILL WORKS
async function importCards(setsData){
  for(let set in setsData){
    const cardData = setsData[set]['cards']

    //remove cards from set so we can save the set info
    delete setsData[set]['cards']

    let newSet = new Sets(setsData[set])
    try{
      await newSet.save()
    }
    catch(error){
      throw new Error('Error saving set: '+error)
    }

    for(let card of cardData){
      card['setCode'] = setsData[set]['code']

      let newCard = new Cards(card)
      try{
        await newCard.save()
      }
      catch(error){
        console.log(error)
        throw new Error('Card save failed')
      }
    }
    console.log('saved set:',newSet.code)
  }// for
}

async function importer(path, cb){
  const data = await readFile(path)
  const setsData = JSON.parse(data)
  try{
    await importCards(setsData)
    console.log('Import finished')
    cb()
  }
  catch(error){
    console.log(error)
    process.exit(1)
  }
}

module.exports = importer
