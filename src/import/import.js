'use strict'
require('babel-register')
require('babel-polyfill')

const mongoose = require('mongoose')
const config = require('../../config')
const importer = require('./importer')

mongoose.connect(`mongodb://${config.DB_USER}:${config.DB_PASS}@${config.DB_URI}`)
mongoose.Promise = Promise
const location = '/home/miika/Downloads/AllSets.json'

importer(location,function(){
  mongoose.connection.close()
})
