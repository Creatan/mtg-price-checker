'use strict'

const mongoose = require('mongoose')

let mkmMapSchema = mongoose.Schema({
  multiverseid: Number,
  mkmid: Number
})


module.exports = mongoose.model('mkmMap', mkmMapSchema)
