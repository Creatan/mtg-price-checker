'use strict'

const mongoose = require('mongoose')
const Sets = require('./sets')

let CardSchema = new mongoose.Schema({
  id: String,
  name: String,
  artist: String,
  cmc: Number,
  colorIdentity: [String],
  colors: [String],
  flavor: String,
  imageName: String,
  layout: String,
  manaCost: String,
  multiverseid: Number,
  power: String,
  toughness: String,
  rarity: String,
  subtypes: [String],
  text: String,
  type: String,
  types: [String],
  setCode: String,
  mkm_id: Number
})

CardSchema.virtual('cardSet',{
  ref: 'Sets',
  localField: 'setCode',
  foreignField: 'code'
})

CardSchema.statics.findByNameAndSet = function(obj, cb){
  return this.find({name: new RegExp(obj.name,'i'), cardSet: obj.set}).populate('cardSet').exec(cb)
}

CardSchema.statics.findByName = function(name, cb){
  const expr = new RegExp("^"+name.replace(/\*/g,'.*')+"$", 'i')
  return this.find({name: {$regex: expr}}).populate('cardSet').exec(cb)
}

module.exports = mongoose.model('Card',CardSchema)
